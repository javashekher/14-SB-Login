package com.ashokit.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ashokit.mvc.service.LoginService;

@Controller
public class LoginController {
	@Autowired
	LoginService  loginService;
	
	@GetMapping( value = "/login" )
	public String getLoginPage() {
		return "Login";
	}
	
	@PostMapping( value = "/signin")
	public String checkSignIn(@RequestParam String username, @RequestParam String password, Model model ) {
		
		boolean flag = loginService.checkUserLogin(username, password);
		if(flag) {
			model.addAttribute("user", username);
			return "Success";
		}
		else {
			model.addAttribute("message", "Bad Credentials");
			return "Login";
		}
		
	}

}
