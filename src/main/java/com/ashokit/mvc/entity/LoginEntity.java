package com.ashokit.mvc.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name ="USER_LOGIN")
@Data
public class LoginEntity {
	@Id
	private String username;
	
	private String password;
	
	private String status;

}
